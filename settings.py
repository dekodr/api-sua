import os
import sys
from environs import Env

env = Env()
env.read_env()

DEBUG = os.getenv('DEBUG', 'False') == 'True'

TESTING = True if 'test' in sys.argv else False
PYTEST = True if 'pytest' in sys.modules else False
SHELL = True if 'shell' in sys.argv else False

SECRET_KEY = env.str('SECRET_KEY', 'imyoursecretkey')
TOKEN_EXPIRY = int(env.str('TOKEN_EXPIRY') or 3600)
REDIS_HOST = env.str('REDIS_HOST', 'localhost')
REDIS_PORT = env.str('REDIS_PORT', '5000')
BASE_URL = 'https://sua.ganloca.co'

GOOGLE_CLIENT_ID = env.str('GOOGLE_CLIENT_ID', '')
GOOGLE_CLIENT_SECRET = env.str('GOOGLE_CLIENT_SECRET', '')
GOOGLE_DISCOVERY_URL = "https://accounts.google.com/.well-known/openid-configuration"

APPS_KEY = env.str('APPS_KEY', 'tanyatuhan')

DATABASE_URI = env.str('DATABASE_URI')

LOGGING_ROOT = "logs"

INFO_LOGGER_NAMES = ['urllib3',
                     'kombu',
                     'amqp',
                     'celery.pool',
                     'celery.bootsteps',
                     'celery.worker.consumer',
                     'Cache<CACHE-INSTANCE-0>',
                     'parso',
                     '/usr/local/lib/python3.7/site-packages/envparse.py',
                     '/usr/local/lib/python3.6/site-packages/envparse.py']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,  # If you declare global level logger.
    'formatters': {
        'json': {
            '()': "pythonjsonlogger.jsonlogger.JsonFormatter",
            'format': "%(asctime)s %(msecs)d %(process)d %(threadName)s %(name)s %(levelname)s %(filename)s:%(lineno)s %(message)s",
            'datefmt': "%Y-%m-%dT%H:%M:%S%z",
            'json_indent': 4 if SHELL or TESTING or PYTEST else None
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'json',
            'stream': 'ext://sys.stdout'
        },
        'file': {
            'level': 'DEBUG',
            'class': 'cloghandler.ConcurrentRotatingFileHandler',
            'maxBytes': 1024 * 1024 * 10,  # 10 MB
            'backupCount': 500,
            'filename': os.path.join(LOGGING_ROOT, 'log.log'),
            'formatter': 'json',
            'encoding': 'utf8'
        }
    },
    'root': {  # this logger is for anything that we don't configure as INFO
        'handlers': ['file', 'console'],
        'level': 'DEBUG',
        'propagate': False
    },
    'loggers': {
        logger_name: {
            'handlers': ['file', 'console'],
            'level': 'INFO',
            'propagate': False
        } for logger_name in INFO_LOGGER_NAMES
    }
}
