from flask import Flask, jsonify
from werkzeug.contrib.cache import RedisCache
from werkzeug.exceptions import HTTPException
from requests.exceptions import RequestException
from requests import Response
from status import statuses
from util import db
from uuid import uuid4
import logging.config
import settings
import copy
from postdata import postdata
from postuser import postuser
from flask_cors import CORS

logging.config.dictConfig(settings.LOGGING)


def format_error(message):
    return {'error': {'message': message}}


def handle_error(error):
    """Try to capture all exception and return as standard error format
    {'error': {'message': message}}

    This will difference for Flask_API if you raise flask_api.exceptions.APIException out.
    There that one will format as {"message":<detail>}
    """
    logger = logging.getLogger(__name__)
    req_id = str(uuid4())
    to_response = format_error('exception from request_id: {}'.format(req_id))
    error_body = dict(
        exception=repr(error)
    )
    if isinstance(error, RequestException):
        status_code = 502
        if isinstance(error.response, Response):
            error_body['target'] = 'channel'
            error_body['url'] = error.response.url
            error_body['code'] = error.response.status_code
            try:
                error_body['body'] = error.response.json()
            except ValueError:
                error_body['body'] = error.response.text
    elif isinstance(error, HTTPException):
        status_code = error.code
    else:
        status_code = 500

    to_response['error'].update(error_body)
    logger.exception(copy.copy(to_response['error']))
    return jsonify(to_response), status_code

def create_app():
    from common.middlewares import LoggingMiddleware
    app = Flask(__name__)
    CORS(app)
    # CORS(app, support_credentials=True)
    # app.config['CORS_HEADERS'] = 'Content-Type'
    app.config['SQLALCHEMY_DATABASE_URI'] = settings.DATABASE_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

    app.config.from_object(settings)
    app.cache = RedisCache(host=settings.REDIS_HOST, port=settings.REDIS_PORT)

    app.register_error_handler(Exception, handle_error)
    app.wsgi_app = LoggingMiddleware(app.wsgi_app)
    app.static_folder = 'static'

    db.init_app(app)

    with app.app_context():
        try:
            db.create_all()
        except Exception as e:
            logging.error({
                "error": "Database Error",
                "messages": e
            })
    
    app.register_blueprint(statuses)
    app.register_blueprint(postdata)
    app.register_blueprint(postuser)

    return app
    