from flask import Blueprint, jsonify

statuses = Blueprint('status', __name__)

@statuses.route('/status', methods=['GET'])
def status():
    return jsonify(message="OK"), 200