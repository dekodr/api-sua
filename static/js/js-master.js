
$( document ).ready( function(){
    
    $('.overlay-back').on('click', function () { 
        $(".hidden-box.login-userid").removeClass('active')
        $(".hidden-box.login-face").removeClass('active')
        $('.section-right .box-left').removeClass('hidden')
    });
    $('.trigger-face').on('click', function () { 
        $(".hidden-box.login-userid").removeClass('active')
        $(".hidden-box.login-face").addClass('active')
        $('.section-right .box-left').addClass('hidden')
    });
    $('.trigger-email').on('click', function () { 
        $(".hidden-box.login-face").removeClass('active')
        $(".hidden-box.login-userid").addClass('active')
        $('.section-right .box-left').addClass('hidden')
    });
    $('.create-meeting-trigger').on('click', function () { 
        $(".create-meeting-frame").addClass('active')
    });
    $('.cancel-meeting-trigger').on('click', function () { 
        $(".create-meeting-frame").removeClass('active')
    });

    $('.jadwal-rapat-trigger').on('click', function () { 
        $(".modal-jadwal-rapat").addClass('active')
        $(".popup-overlay").addClass('active')
    });
    $('.modal-close-trigger').on('click', function () { 
        $(".modal").removeClass('active')
        $(".popup-overlay").removeClass('active')
    });


    // POP UP SETTING //

    $('.popup-close-trigger').on('click', function () { 
        $(".popup-master").removeClass('active')
        $(".popup-overlay").removeClass('active')
    });
    $('.invite-participant-trigger').on('click', function () { 
        $(".popup-master.invite-participant").addClass('active')
        $(".popup-overlay").addClass('active')
    });

    // LOGIN STYLE 2 //

    $('.login-trigger').on('click', function () { 
        $(".hidden-frame.login").addClass('active')
    });
    $('.gabung-rapat-trigger').on('click', function () { 
        $(".hidden-frame.gabung-rapat").addClass('active')
    });
    $('.back-to-login').on('click', function () { 
        $(".hidden-frame").removeClass('active')
    });

    var x, i, j, l, ll, selElmnt, a, b, c;

    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("select-frame");
    l = x.length;
    for (i = 0; i < l; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    ll = selElmnt.length;
    /*for each element, create a new DIV that will act as the selected item:*/
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /*for each element, create a new DIV that will contain the option list:*/
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < ll; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h, sl, yl;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            sl = s.length;
            h = this.parentNode.previousSibling;
            for (i = 0; i < sl; i++) {
            if (s.options[i].innerHTML == this.innerHTML) {
                s.selectedIndex = i;
                h.innerHTML = this.innerHTML;
                y = this.parentNode.getElementsByClassName("same-as-selected");
                yl = y.length;
                for (k = 0; k < yl; k++) {
                y[k].removeAttribute("class");
                }
                this.setAttribute("class", "same-as-selected");
                break;
            }
            }
            h.click()
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function(e) {
        /*when the select box is clicked, close any other select boxes,
        and open/close the current select box:*/
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
        });
    }
    function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, xl, yl, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    xl = x.length;
    yl = y.length;
    for (i = 0; i < yl; i++) {
        if (elmnt == y[i]) {
        arrNo.push(i)
        } else {
        y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < xl; i++) {
        if (arrNo.indexOf(i)) {
        x[i].classList.add("select-hide");
        }
    }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);

// navBAR screenshare toggle //

$(".sharescreen-toggle").click(function() {
    $(this).toggleClass("active");
});

// SIDE BAR TRIGGER //

    $('.sidebar-chat-trigger').on('click', function () { 
        $('.sidebar-chat-trigger').addClass('active')
        $(".meeting-screen").addClass('sidebar-active')
        $(".meeting-screen-sidebar ").addClass('active')
        $(".body-content.chat").addClass('active')
        $(".sidebar-participant-trigger").removeClass('active')
        $(".body-content.participant").removeClass('active')
    });
    $('.sidebar-participant-trigger').on('click', function () { 
        $('.sidebar-participant-trigger').addClass('active')
        $(".meeting-screen").addClass('sidebar-active')
        $(".meeting-screen-sidebar ").addClass('active')
        $(".body-content.participant").addClass('active')
        $(".body-content.chat").removeClass('active')
        $(".sidebar-chat-trigger").removeClass('active')
    });
    $('.sidebar-close').on('click', function () { 
        $(".meeting-screen-sidebar ").removeClass('active')
        $(".meeting-screen").removeClass('sidebar-active')
    });
});

