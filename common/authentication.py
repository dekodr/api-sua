from flask import current_app, request, jsonify, g
from passlib.context import CryptContext
from models import Username
from werkzeug.exceptions import Unauthorized, HTTPException
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask_api.exceptions import AuthenticationFailed, NotAuthenticated, ParseError
from itsdangerous import BadSignature, SignatureExpired
from functools import wraps

class Encryption:
    def __init__(self):
        self.pwd_context = CryptContext(
                schemes=["pbkdf2_sha256"],
            default="pbkdf2_sha256",
            pbkdf2_sha256__default_rounds=30000
        )
    
    def encrypt_password(self, password):
        return self.pwd_context.encrypt(password)

    def check_encrypted_password(self, password, hashed):
        return self.pwd_context.verify(password, hashed)

def token_required(func):
    @wraps(func)
    def check_token(*args, **kwargs):
        authorization = request.headers.get('Authorization')
        if not authorization:
            raise NotAuthenticated

        try:
            token = authorization.split(' ')[1]
        except IndexError:
            raise AuthenticationFailed('Invalid Token')

        try:
            g.token_data = verify_auth_token(token)
        except HTTPException as e:
            return jsonify(message=e.description), e.code

        return func(*args, **kwargs)

    return check_token


def generate_auth_token(data):
    key = current_app.config.get('APPS_KEY')
    apps_key = data.get('apps_key')
    print(key)
    print(apps_key)
    if apps_key != key:
        raise Unauthorized('Invalid Apps Key')

    s = Serializer(current_app.config['SECRET_KEY'],
                   expires_in=current_app.config['TOKEN_EXPIRY'])

    return s.dumps(data).decode('utf-8')

def verify_auth_token(token):
    s = Serializer(current_app.config['SECRET_KEY'])
    try:
        return s.loads(token)
    except SignatureExpired:
        raise AuthenticationFailed('Token Expired')
    except BadSignature:
        # invalid token
        raise ParseError('Invalid Token')
