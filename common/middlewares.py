"""Module for any WSGI middleware
"""
import logging
from flask import current_app
import os

ALLOWED_IMAGE_DATA = set(['jpg','jpeg','png','gif'])

class LoggingMiddleware():
    """Basic log middleware to log url and response code.
    If you want more sophisticate level, use
    http://flask.pocoo.org/docs/1.0/api/#flask.Flask.before_request
    http://flask.pocoo.org/docs/1.0/api/#flask.Flask.after_request
    """
    def __init__(self, wsgi_app):
        self._wsgi_app = wsgi_app
        self._logger = logging.getLogger(__name__)

    def __call__(self, environ, start_response):
        self._logger.debug("Request: %s", environ.get("PATH_INFO"))

        def log_response(status, headers, *args):
            self._logger.debug("Response: %s, headers: %s", status, headers)
            return start_response(status, headers, *args)

        return self._wsgi_app(environ, log_response)

def allowed_file_image(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_IMAGE_DATA
