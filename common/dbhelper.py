from models import *
from app import db
from datetime import datetime


def postmeeting(id_user='', meeting_id = '', passcode = '', name = '', start_date = '', start_time = '', end_time = '', end_date = ''):
    data = MsMeeting(   id_user=id_user,
                        meeting_id=meeting_id,
                        passcode=passcode,
                        name=name,
                        start_date=start_date,
                        start_time=start_time,
                        end_time=end_time,
                        end_date=end_date,
                        delete=0)
    db.session.add(data)
    db.session.commit()
    return data
