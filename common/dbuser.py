from models import MsUser, Username, Password
from app import db
from datetime import datetime
from common.authentication import Encryption

def postuser(name, email, photo_profile, username, password, google_unique_id=None, fb_unique_id=None):
    password = Encryption().encrypt_password(password)
    newmsuser = MsUser(name=name, photo_profile=photo_profile, google_unique_id=google_unique_id, fb_unique_id=fb_unique_id, email=email, delete=0)
    db.session.add(newmsuser)
    db.session.commit()
    newusername = Username(id_user=newmsuser.id, username=username, is_active=1, delete=0)
    newpassword = Password(id_user=newmsuser.id, password=password, is_active=1, delete=0)
    db.session.add(newusername)
    db.session.add(newpassword)
    db.session.commit()
    return newmsuser

def edituser(name, email, phone, username, password, is_active, id):
    useredit = MsUser.query.filter_by(id=id).first()
    useredit.name = name
    useredit.email = email
    useredit.phone = phone
    useredit.edit_date = datetime.utcnow()
    db.session.commit()
    usernameedit = Username.query.filter_by(id_user=id).first()
    usernameedit.username = username
    usernameedit.is_active = is_active
    db.session.commit()
    passwordedit = Password.query.filter_by(id_user=id).first()
    passwordedit.password = password
    passwordedit.is_active = is_active
    db.session.commit()
    return True

def deleteuser(id):
    useredit = MsUser.query.filter_by(id=id).first()
    useredit.delete = 1
    usernameedit = Username.query.filter_by(id_user=id).first()
    usernameedit.delete = 1
    passwordedit = Password.query.filter_by(id_user=id).first()
    passwordedit.delete = 1
    db.session.commit()
    return True

def checkuserlogin(username):
	#res = db.session.query(AdminModel.password, AdminModel.id_merchant).filter(AdminModel.username == username).first()
    ceklogin = MsUser.query.join(Username).filter(Username.username == username).first()
    # ceklogin = db.session.query(MsUser.usernameModel.username, MsUser.passwordModel.password).filter_by(MsUser.usernameModel.username == username).first()
    #ceklogin = db.session.query(MsUser.passwordModel.password == password).filter(MsUser.usernameModel.username == username).first()
    #ceklogin = MsUser.query.filter_by(MsUser.usernameModel.username == username).first()
    # ceklogin = db.session.query(MsUser.passwordModel.password).filter_by(MsUser.usernameModel.username == username).first()
    return ceklogin

def getuser(id):
    user = MsUser.query.filter_by(id=id).first()
    
    return user.id_boss

def checkusergoogle(google_unique_id):
    checkuser = MsUser.query.filter_by(google_unique_id=google_unique_id).first()
    return checkuser