from marshmallow import Schema, fields


class MeetingPostSchema(Schema):
    id_user = fields.Integer(
        required=True,
        allow_none=False,
        error_messages={'required': 'id_user is required'}
    )
    meeting_id = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'meeting_id is required'}
    )
    name = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'name is required'}
    )
    
class MeetingScheduleSchema(Schema):
    id_user = fields.Integer(
        required=True,
        allow_none=False,
        error_messages={'required': 'id_user is required'}
    )
    meeting_id = fields.Integer(
        required=True,
        allow_none=False,
        error_messages={'required': 'meeting_id is required'}
    )
    passcode = fields.String(
        required=False,
        allow_none=True,
        error_messages={'required': 'passcode is required'}
    )
    name = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'name is required'}
    )
    start_date = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'start_date is required'}
    )
    start_time = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'start_time is required'}
    )
    end_date = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'end_date is required'}
    )
    end_time = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'end_time is required'}
    )

class UserPostsSchema(Schema):
    name = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'name is required'}
    )
    email = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'email is required'}
    )
    photo_profile = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'photo_profile is required'}
    )
    username = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'username is required'}
    )
    password = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'password is required'}
    )

class LoginUserSchema(Schema):
    type = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'type is required'}
    )
    username = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'username is required'}
    )
    password = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'password is required'}
    )
    apps_key = fields.String(
        required=True,
        allow_none=False,
        error_messages={'required': 'apps_key is required'}
    )