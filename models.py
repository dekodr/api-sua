from datetime import datetime
from app import db


class MsMeeting(db.Model):
    __tablename__ = 'ms_meeting'
    id              = db.Column(db.Integer, primary_key=True)
    id_user         = db.Column(db.Integer())
    meeting_id      = db.Column(db.String())
    passcode        = db.Column(db.String())
    name            = db.Column(db.String())
    start_date      = db.Column(db.String())
    start_time      = db.Column(db.String())
    end_time        = db.Column(db.String())
    end_date        = db.Column(db.String())
    delete          = db.Column(db.Integer())


class MsUser(db.Model):
    __tablename__ = 'ms_user'
    id              = db.Column(db.Integer, primary_key=True)
    google_unique_id= db.Column(db.String())
    fb_unique_id    = db.Column(db.String())
    usernameModel   = db.relationship('Username', uselist=False)
    passwordModel   = db.relationship('Password', uselist=False)
    name            = db.Column(db.String())
    email           = db.Column(db.String())
    photo_profile   = db.Column(db.String())
    photo_id        = db.Column(db.String())
    number_id       = db.Column(db.String())
    division        = db.Column(db.String())
    position        = db.Column(db.String())
    delete          = db.Column(db.Integer())

class Username(db.Model):
    __tablename__ = 'tr_username'
    id          = db.Column(db.Integer, primary_key=True)
    id_user     = db.Column(db.Integer, db.ForeignKey('ms_user.id'))
    username    = db.Column(db.String())
    is_active   = db.Column(db.Boolean())
    delete      = db.Column(db.Integer())

class Password(db.Model):
    __tablename__ = 'tr_password'
    id          = db.Column(db.Integer, primary_key=True)
    id_user     = db.Column(db.Integer, db.ForeignKey('ms_user.id'))
    password    = db.Column(db.String())
    is_active   = db.Column(db.Boolean())
    delete      = db.Column(db.Integer())
