from flask import Blueprint, request, jsonify, Flask, render_template, flash, redirect, url_for, current_app, make_response
from serializers import LoginUserSchema, UserPostsSchema
from app import db
from models import *
from common.dbuser import *
from common.authentication import Encryption, generate_auth_token
from marshmallow import ValidationError
from werkzeug.exceptions import Unauthorized
from oauthlib.oauth2 import WebApplicationClient
from time import sleep
import settings
import os
import requests
import json

client = WebApplicationClient(settings.GOOGLE_CLIENT_ID)

postuser = Blueprint('postuser', __name__)

def get_google_provider_config():
    return requests.get(settings.GOOGLE_DISCOVERY_URL).json()

@postuser.route('/user/add', methods=['POST', 'GET'])
def userposts():
    if request.method == 'POST':
        try:
            payload = request.get_json() or {}
            data, _ = UserPostsSchema(strict=True).load(payload)
        except ValidationError as e:
            return jsonify(message=e.description), 422
        
        current_app.logger.debug(dict(
            message="=== Request Add User ===",
            data=data
        ))

        result = postuser(data['name'], data['email'], data['photo_profile'], data['username'], data['password'])
        return jsonify(message="User has been created successfully"), 200

@postuser.route('/login-user-google')
def login_with_google():
    google_provider = get_google_provider_config()
    auth_endpoint = google_provider['authorization_endpoint']

    req = client.prepare_request_uri(
        auth_endpoint,
        redirect_uri= settings.BASE_URL + "/login-user-google/callback",
        scope=["openid", "email", "profile"],
    )

    return redirect(req)

@postuser.route('/login-user-google/callback')
def callback():
    data = request.args.get('code')
    google_provider = get_google_provider_config()
    token_endpoint = google_provider['token_endpoint']
    auth_response = request.url

    token_url, headers, body = client.prepare_token_request(
        token_endpoint,
        authorization_response=auth_response.replace('http', 'https'),
        redirect_url=settings.BASE_URL + '/login-user-google/callback',
        code=data
    )

    token_response = requests.post(
        token_url,
        headers=headers,
        data=body,
        auth=(settings.GOOGLE_CLIENT_ID, settings.GOOGLE_CLIENT_SECRET)
    )

    client.parse_request_body_response(json.dumps(token_response.json()))

    userinfo_endpoint = google_provider['userinfo_endpoint']
    uri, headers, body = client.add_token(userinfo_endpoint)
    userinfo_response = requests.get(uri, headers=headers, data=body)

    if userinfo_response.json().get('email_verified'):
        name = userinfo_response.json()['name']
        unique_id = userinfo_response.json()['sub']
        user_email = userinfo_response.json()['email']
        picture = userinfo_response.json()['picture']
        user_name = userinfo_response.json()['given_name']
    else:
        return jsonify(message="User email not available or not verified by Google"), 400
    
    exists = checkusergoogle(unique_id)
    if not exists:
        res = postuser(name, user_email, picture, user_name, '', unique_id, '')
    
    login_data = {
        'username': user_name,
        'password': '',
        'type': 'google',
        'apps_key': current_app.config.get('APPS_KEY')
    }

    token = generate_auth_token(login_data)
    search_id_user = Username.query.filter_by(username=login_data['username']).first()
    search_name = MsUser.query.filter_by(id=search_id_user.id_user).first()

    sua_url = f'sua://google/{token}/{login_data["username"]}/{search_id_user.id_user}/{search_name.name}/{search_name.position}'
    return render_template('page-login-google.html', sua=sua_url)

@postuser.route('/login-user', methods=['GET', 'POST'])
def login_dashboard():
    try:
        payload = request.get_json() or {}
        data, _ = LoginUserSchema(strict=True).load(payload)

        current_app.logger.debug(dict(
            message="=== Request Login User ===",
            data=data
        ))

        if data['type'] == 'web':
            check_user = checkuserlogin(data['username'])
            if check_user is None:
                return jsonify(message="User Not Found"), 404
            
            passwd = Encryption().check_encrypted_password(data['password'], "$pbkdf2-sha256$30000$VMoZg/D.nzOG0HpPiREi5A$UEIrbQ/qbZkevWoxoicYuUci.F1KpRvSx9ehwN.j7gA")
            if passwd is False:
                return jsonify(message="User or Password not Match"), 404
            
            token = generate_auth_token(data)

            search_id_user = Username.query.filter_by(username=data['username']).first()
            search_name = MsUser.query.filter_by(id=search_id_user.id_user).first()
    except ValidationError as e:
        return jsonify(message=e.description), 422
    except Unauthorized as e:
        return jsonify(message=e.description), e.code
    
    return jsonify(token=token, username=data['username'], id_user=search_id_user.id_user, 
    name=search_name.name, position=search_name.position), 200
   