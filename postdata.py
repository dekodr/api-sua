from flask import Blueprint, request, jsonify, current_app, render_template
from common.dbhelper import *
from common.dbuser import *
from models import *
from serializers import MeetingPostSchema, MeetingScheduleSchema
from common.authentication import Encryption, generate_auth_token, token_required
from common.middlewares import allowed_file_image, ALLOWED_IMAGE_DATA
from werkzeug.utils import secure_filename
from werkzeug.exceptions import Unauthorized
from marshmallow import ValidationError

postdata = Blueprint('postdata', __name__)


@postdata.route('/meeting/post', methods=['POST', 'GET'])
def meetingpost():
    if request.method == 'POST':
        try:
            payload = request.get_json() or {}
            data, _ = MeetingPostSchema(strict=True).load(payload)
        except ValidationError as e:
            return jsonify(e.messages), 422
        
        current_app.logger.debug(dict(
            message="=== Request Meeting Post ===",
            data=data
        ))

        result = postmeeting(data['id_user'], data['meeting_id'], data['name'], 
            data['start_date'], data['start_time'], data['end_time'], data['end_date'])
        
        return jsonify(message="Meeting has been created successfully"), 200

@postdata.route('/meeting/schedule/post', methods=['POST', 'GET'])
def meetingschedulepost():
    if request.method == 'POST':
        try:
            payload = request.get_json() or {}
            data, _ = MeetingScheduleSchema(strict=True).load(payload)
        except ValidationError as e:
            return jsonify(e.messages), 422
        
        current_app.logger.debug(dict(
            message="=== Request Meeting Post Schedule ===",
            data=data
        ))

        result = postmeeting(data['id_user'], data['meeting_id'], data['passcode'], data['name'], 
            data['start_date'], data['start_time'], data['end_time'], data['end_date'])
        
        return jsonify(message="Meeting schedule has been created successfully"), 200

@postdata.route('/meeting/schedule/get/<int:id_user>/<int:per_page>/<int:page_num>', methods=['GET'])
@token_required
def scheduleget(id_user, per_page, page_num):
    if id_user > 0:
        pagination = MsMeeting.query.filter(MsMeeting.delete == 0, MsMeeting.id_user == id_user).paginate(
            per_page=per_page, page=page_num, error_out=True)
    else:
        pagination = MsMeeting.query.filter_by(delete=0).paginate(per_page=per_page, page=page_num, error_out=True)

    result = [
        {
            'name': i.name,
            'start_date': i.start_date,
            'end_date': i.end_date,
            'meeting_id': i.meeting_id
        } for i in pagination.items
    ]

    return jsonify(schedule=result), 200

@postdata.route('/meeting/get/<string:meeting_id>', methods=['GET'])
@token_required
def meetingget(meeting_id):
    data = MsMeeting.query.filter_by(meeting_id=meeting_id).first()
    result = {
        'name': data.name,
        'start_date': data.start_date,
        'end_date': data.end_date,
        'meeting_id': data.meeting_id,
        'passcode': data.passcode
    }
    return jsonify(meeting=result), 200

@postdata.route('/meet/me/sua_app/<string:meeting_id>', methods=['GET'])
def meetingshare(meeting_id):
    if request.method == 'GET':

        # sua_url = f'sua://meet/{meeting_id}/'
        sua_url = "sua://meet/"+meeting_id
        return render_template('page-open-meeting.html', sua=sua_url)
